<!DOCTYPE html>
<?php
$this->load->library('session');
$name=$this->session->userData('name');
$user_type = $this->session->userData('user_type');
$logged_in = $this->session->userData('logged_in');
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>File Storage</title>

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/dist/css/adminlte.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
  <!-- Generic page styles -->


  <!-- blueimp Gallery styles -->
  <link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
  <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.fileupload.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.fileupload-ui.css">
  <!-- CSS adjustments for browsers with JavaScript disabled -->
  <noscript><link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.fileupload-noscript.css"></noscript>
  <noscript><link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.fileupload-ui-noscript.css"></noscript>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="<?php echo base_url();?>assets/css/font-wso2.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/dataTables.listView.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

</head>
<body class="hold-transition sidebar-mini ">
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar fixed-top navbar-expand bg-light navbar-light border-bottom m-0 ">
      <!-- Left navbar links d-lg-none d-md-block -->
      <ul class=" navbar-nav">
        <li class="nav-item">
          <a class="nav-link d-flex" data-widget="pushmenu" href="#">
            <i class="fas fa-bars align-self-center"></i>
          </a>
        </li>
      </ul>



      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">

        <!-- Notifications Dropdown Menu -->

        <li class="nav-item dropdown">
          <a class="nav-link d-flex" data-toggle="dropdown" href="#">
            <i class="far fa-user align-self-center"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
              <i class="fas fa-user fa-fw"></i> User Profile
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-cog fa-fw"></i> Settings
            </a>
            <div class="dropdown-divider"></div>
            <a href="<?php  echo site_url('login/logout'); ?>" class="dropdown-item">
              <i class="fas fa-lock fa-fw"></i> Logout
            </a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link d-flex" data-widget="control-sidebar" data-slide="true" href="#">
            <i class="far fa-bell align-self-center"></i>
            <span class="badge badge-pill badge-success navbar-badge">3</span>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar bg-light h-100 border-right"  style="top: 57px;">
      <!-- Brand Logo -->
      <!-- <a href="<?php //echo site_url('user'); ?>" class="brand-link">
      <img src="<?php //echo base_url();?>/assets/admin/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light" style="color: #000;">File Storage</span>
    </a> -->

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <!-- <div class="image">
        <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div> -->
      <div class="info">
        <a href="<?php echo site_url('user'); ?>" class="d-block" style="color: #000; text-transform: uppercase;font-size: 24px; letter-spacing: 2px;"><?=$name?></a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
        with font-awesome or any other icon font library -->

        <?php
        if($user_type=='super'){
          ?>
          <li class="nav-item ">
            <a href="<?php echo site_url('redirect/user_list'); ?>" class="nav-link">
              <i class="nav-icon far fa-user"></i>
              <p>
                User List
              </p>
            </a>
          </li>
        <?php } ?>

        <li class="nav-item ">
          <a href="<?php echo site_url('user/my_file'); ?>" class="nav-link">
            <i class="nav-icon far fa-file"></i>
            <p>My Files </p>
          </a>
        </li>


        <li class="nav-item ">
          <a href="<?php echo site_url('user/all_file'); ?>" class="nav-link">
            <i class="nav-icon far fa-file"></i>
            <p>All Files </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
