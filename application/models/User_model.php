<?php

class User_Model extends CI_Model{

    function __construct(){

        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    //insert employee details to employee table
    public function insertUser($data){

        return $this->db->insert('user',$data);

    }
    public function get_users()
     {
          return $this->db->get("user");
     }
    public function loginUser($username, $password){
        //$this->db->where(array('username' = >$username, 'password' => $password));
        // var_dump($password);die();
        $query = $this->db->get_where('user', array('name' => $username, 'password' => $password));

        if($query->num_rows() == 1){

            $userArr = array();
            foreach($query->result() as $row){
                $userArr[0] = $row->id;
                $userArr[1] = $row->name;
                $userArr[2] = $row->user_type;

            }
           $userData = array(
                'id' => $userArr[0],
                'name' => $userArr[1],
                'user_type' => $userArr[2],
                'logged_in'=> TRUE
            );
            $this->session->set_userdata($userData);

            return $query->result();
        }

        else{
            return false;
        }
    }

     public function get_email($email){

              $this -> db -> select('*');
          		$this -> db -> from('user');
          		$this -> db -> where('email = ' . "'" . $email . "'" );
          		$query = $this->db->get();
          		$res = $query->result();
          	 if(sizeof($res) > 0){
               $r = $query->result();
               //var_dump($r);die();
               $user=$r[0];
               $this->resetpassword($user);
               $info= "Password has been reset and has been sent to email id: ". $email;
               redirect('/login/forget?info=' . $info, 'refresh');
             }
             else{
               $error= "The email id you entered not found on our database ";
               redirect('/login/forget?error=' . $error, 'refresh');
             }

        }

        private function resetpassword($user)
        {
            date_default_timezone_set('GMT');
            $this->load->helper('string');
            $password= random_string('alnum', 16);
            $this->db->where('id', $user->id);
            $this->db->update('user',array('password'=>MD5($password)));
            $this->load->library('email');
            $this->email->from('deathkanji@gmail.com', 'Media repo');
            $this->email->to($user->email);
            $this->email->subject('Password reset');
            $this->email->message('You have requested the new password, Here is you new password:'. $password);
            $this->email->send();
        }




}

?>
