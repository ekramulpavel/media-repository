<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct(){
        parent::__construct();

         $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');

        $this->load->helper('url');
         $this->load->database();
        $this->load->model('User_model');
    }

    public function user_list()
	{
		$this->load->view('admin/header');
        $this->load->view('admin/user_list');
        $this->load->view('admin/footer');
	}
    function signup() {
            $this->form_validation->set_rules('txt_name','name', 'required');
            $this->form_validation->set_rules('txt_email','User email', 'required');
            $this->form_validation->set_rules('txt_password','Password', 'required');
            $this->form_validation->set_rules('txt_confirm_password', 'Password Confirmation', 'trim|required|matches[txt_password]');

            if ($this->form_validation->run() == FALSE) {
                // var_dump("hi");die();
                 redirect('user/user_list');
            } else {
            //Setting values for tabel columns
            $data = array(
                'name' => $this->input->post('txt_name'),
                'email' => $this->input->post('txt_email'),
                'password' => md5($this->input->post('txt_password')),
                'user_type' =>$this->input->post('txt_type'),
            );
            //Transfering data to Model
            $this->User_model->insertUser($data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data Inserted Successfully </div>');
            // //Loading View
            $this->load->view('admin/header');
            $this->load->view('admin/user_list',$data);
            $this->load->view('admin/footer');
            }
        }


        public function show_user() {

          // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));


          $users = $this->User_model->get_users();

          $data = array();

          foreach($users->result() as $r) {

               $data[] = array(
                    $r->id,
                    $r->name,
                    $r->email,
                    '<a href="#" class="btn-sm btn-info mr-2">Edit</a><a href="#" class="btn-sm btn-danger">Delete</a>'

               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $users->num_rows(),
                 "recordsFiltered" => $users->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
     }

     public function my_file()
     {
         $this->load->view('admin/header');
         $this->load->view('admin/my_file');
         $this->load->view('admin/footer');
     }

      public function all_file()
     {
         $this->load->view('admin/header');
         $this->load->view('admin/all_file');
         $this->load->view('admin/footer');
     }
     public function index(){


        if(isset($_SESSION['id'])){
            // echo $_SESSION['id'];
            $this->load->view('admin/header');
            $this->load->view('admin/admin');
            $this->load->view('admin/footer');
        }else{
            redirect('login/index');
        }

<<<<<<< HEAD
  public function show_user() {

    // Datatables Variables
    $draw = intval($this->input->get("draw"));
    $start = intval($this->input->get("start"));
    $length = intval($this->input->get("length"));


    $users = $this->User_model->get_users();

    $data = array();
    $user_id = $this->session->userData('id');

    foreach($users->result() as $r) {
      $del_string=($user_id!=$r->id)?'<a class="btn-danger btn-sm text-light ml-2 delete" data-value="'.site_url() .'user/delete?id='.$r->id .'">Delete</a>':'';

      $data[] = array(
        $r->id,
        $r->name,
        $r->email,
        '<a class="btn-primary btn-sm text-light editmodal" data-toggle="modal" data-name="'.$r->name.'" data-email="'.$r->email .'"  data-id="'.$r->id .'" data-target="#EditFormModal">Edit</a>'.$del_string.''

      );
    }

    $output = array(
      "draw" => $draw,
      "recordsTotal" => $users->num_rows(),
      "recordsFiltered" => $users->num_rows(),
      "data" => $data
    );
    echo json_encode($output);
    exit();
  }

  public function my_file()
  {
    if(isset($_SESSION['id'])){
      $this->load->view('admin/header');
      $this->load->view('admin/my_file');
      $this->load->view('admin/footer');
    }else{
      redirect('login/index');
=======
>>>>>>> 1130223f9ea3c610ac3da0fd0e53f379d6b5fac7
    }

    public function logout(){

        if($this->session->has_userdata('id')){
            //$this->session->unset_userdata('user_data');
            $this->session->sess_destroy();
            //unset($_SESSION['user_data']);
            echo $_SESSION['id'];
            redirect('admin_controller');
        }


    }




}
