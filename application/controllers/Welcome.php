<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see https://codeigniter.com/user_guide/general/urls.html
	*/
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function json(){
		$arr = array(
			'data' => array(
				array(
					'ID' => '1',
					'User_Name' => 'Rashed Ahmed',
					'File_Name' => 'Tiger Nixon.mp3',
					'Date' => '2011/04/25'
				),
				array(
					'ID' => '2',
					'User_Name' => 'Rashed Ahmed',
					'File_Name' => 'Garrett Winters.pdf',
					'Date' => '2011/07/25'
				),
				array(
					'ID' => '3',
					'User_Name' => 'Rashed Ahmed',
					'File_Name' => 'Ashton Cox.ai',
					'Date' => '2011/07/25'
				),
				array(
					'ID' => '4',
					'User_Name' => 'Rashed Ahmed',
					'File_Name' => 'Cedric Kelly.mov',
					'Date' => '2011/07/25'
				),
				array(
					'ID' => '5',
					'User_Name' => 'Rashed Ahmed',
					'File_Name' => 'Airi Satout.dir',
					'Date' => '2011/07/25'
				)
				,
				array(
					'ID' => '6',
					'User_Name' => 'Rashed Ahmed',
					'File_Name' => 'Brielle Williamson.png',
					'Date' => '2011/07/25'
				),
				array(
					'ID' => '7',
					'User_Name' => 'Rashed Ahmed',
					'File_Name' => 'Herrod Chandler.zip',
					'Date' => '2012/12/02'
				)
				,
				array(
					'ID' => '8',
					'User_Name' => 'Rashed Ahmed',
					'File_Name' => 'Rhona Davidson.aac',
					'Date' => '2010/10/14'
				),
				array(
					'ID' => '9',
					'User_Name' => 'Rashed Ahmed',
					'File_Name' => 'Colleen Hurst.doc',
					'Date' => '2009/09/15'
				),
				array(
					'ID' => '10',
					'User_Name' => 'Rashed Ahmed',
					'File_Name' => 'Sonya Frost.wmv',
					'Date' => '2008/12/13'
				)
			)
		);

		echo json_encode($arr);
	}
}
